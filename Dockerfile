FROM swift

WORKDIR /usr/src/app
COPY . ./
RUN swift build -c release


FROM swift:slim

WORKDIR /usr/local/bin
COPY --from=builder /usr/src/app/app .
EXPOSE 8080

CMD [".build/x86_64-unknown-linux/release/app"]

